---
date: '2024'
...

# Overview

The Graph Explorer plugin aims to enable the usage of additional graphs in GroIMP. Therefore two approaches are presented: the GraphObject and the Secondary Graph, which fulfill slightly different needs. Both objects are managed similar to GroIMP resources (such as shaders or images, ...). The plugin also includes explorers for them.

**Please note that the plugin is still a work in progress.**


# GraphObject

The GraphObject can be used to add external files (such as DTD, XEG, PLY) as graphs in a GroIMP project. The import supports the same formats as 'Objects/insert ...'.

The GraphObject is lazily loaded and the object behind it is only resolved once. The GraphObject can then be added to the project graph several times, even between compilations, and will not be reloaded.  

The imported files are saved as files in the GroIMP project without changing them. They are only loaded as resources and can't be changed.



## Explorer

Using the explorer files can be imported with the menu and GraphObjects can be deleted by select and delete.
The explorer can be found on *panels/explorers/graphobjects*. 

<!--[see here](command:showpanel?/ui/panels/graphobjects) -->

Selecting a GraphObject allows you to open it in the 2d or 3d view.

## Usage in RGG

To use an ObjectGraph in RGG the ObjectGraphRef element was created, which as most other reference objects (eg. datasets) gets the name of the object as a parameter as follows: 

```java 
GraphObjectRef gufi = new GraphObjectRef("gufi1.dtd");

```

This new object can than be used to clone its graph and add it to the main graph, for example directly in stead of the axiom, but also at any position and as often as needed.

```java 
Axiom ==> gufi.cloneGraph();
```

Afterwards, the content of the cloned graph is exactly as if it was imported with objects/import File. 

An example can be found in the *examples:Graph Explorer/GraphObjectRef*


Additionally, the GraphObject can also be linked to the simulation as an instance, similar to the Object Explorer or an instantiation rule.

```java
Axiom ==> gufi;
```

In this case, the nodes of the GraphObject are not added to the project graph.  

# Secondary Graphs

The SecGraph object extends the GraphObject, by adding the ability to apply XL queries and RGG functions on the graph and save them. Yet therefore a SecGraph is always saved as an XML file and the original file is not included in the Project.
 
A SecGraph can either be a referencable resource or a variable, this difference is shown by two classes that both extend the SecGraph interface: SecGraphRef and SecGraphImpl.
 
SecGraphRef works similarly to GraphObjectRef only that with the saving of the project the current graph structure is written down in the referenced file. A SecGraphImpl creates a variable, that is saved in a hidden file if it is a attribute of an node, so that the node can be recreated if the project is reopened.
If a SecGraph is added as an attribute to a Node, this SecGraph can be selected though the attribute editor of the node. (A button with the name of the secGraph variable). 



## Explorer

Using the explorer files can be imported with the menu and SecGraphs can be deleted by select and delete. Moreover by selecting on a item the represented SecGraph can be opened in the 2d graph view or the 3d view. Using teh 3d view it is also possible to export the secondary graph to any file supported by GroIMP.


### Multi-file import

It is possible to import multiple files at once **But only if they are all of the same type/format!**


## Usage in RGG



### Initialization

There are several ways to initialize a SecGraph depending on the data that should be included:

(*Since a SecGraph can be either a variable or a reference both are explained below*)

**From an existing Subgraph:**


```java
SecGraph sgr = new SecGraphRef("secGraph1",first((*RGGRoot*)));
SecGraph sgi = new SecGraphImpl(workbench(),first((*RGGRoot*)));
```

This creates a SecGraph that contains a clone of the subgraph below the RGGRoot. This would work with any node. 

**From an existing SecGraph or GraphObject**

```java
SecGraph sgr = new SecGraphRef("secGraph1",sgi);
SecGraph sgi = new SecGraphImpl(workbench(),gufi);
```

This creates a SecGraph that contains a clone of a given GraphObject or SecGraph. This allows users to duplicate graphs or to save a SecGraphImpl in a SecGraphRef.

**Empty/with previous content**

Using the constructors without an additional argument creates the object without adding anything. If a reference was used (SecGraphRef) this loads the old source, if a variable is created an empty graph is provided.

```java
SecGraph sgr = new SecGraphRef("secGraph1");
SecGraph sgi = new SecGraphImpl(workbench());
```

To create an initial graph structure (and overwrite any existing old one) the following syntax can be used: (similar to the one of instantiations):

```java
sgi ==> F [RL(30) F] F;
```

### Current RGGGraph 

while execution RGG and XL function GroIMP is always considering one graph as the current graph. without this plugin that graph is always the ProjectGraph (the one that holds the simulation). Yet with this plugin it is possible to change the current graph during the execution of a rgg function. This is done by the setCurrent() and releaseCurrent() function as shown below.
In the following example the first and the third `println((*F))` would list all F from the project graph while the second one list the F from the SecGraph "secGraph1". Moreover after running this the angle of RL in secGraph1 would increase by 10.

```java

public void example(){
 SecGraphRef sgr1 = new SecGraphRef("secGraph1"); // get the reference to the secondary graph
    println((*F*)); // get all F's on the project graph (the current RGGGraph) 
 sgr1.setCurrent();  // set secGraph1 to be the current RGGGraph
        println((*F*)); // get all F's on secGraph1 (the current RGGGraph) 
 [
 RL(x) ==> RL(x+10); // apply a rule on the current RGGGraph: secGraph1
 ]
 sgr1.releaseCurrent(); // return to the previous RGGGraph: the ProjectGraph
    println((*F*));// get all F's on the project graph (the current RGGGraph) 
}

```

This feature can be nested on several secondary graphs:

```java
 SecGraphRef sg1 = new SecGraphRef("sg1");
 SecGraphRef sg2 = new SecGraphRef("sg2");
 sg1.setCurrent();
 //execute on sg1
 sg2.setCurrent();
 //execute on sg2
 sg2.releaseCurrent();
 //execute on sg1
 sg1.releaseCurrent();	

```

Even so the current graph is changing, the java environment is not effected by this therefore it is possible use the same variables though the howl code. Only with the limitation that a Node can always only be added to one graph.

Two examples about that can be found: one about querying existing graph structure: *examples:Graph Explorer/SecGraphQueryExample* and on created secondaray graphs: *examples:Graph Explorer/SecGraphCreationExample* .


### run RGG/XL functions

The Other way of executing on a certain secondary graph is with the apply("functionName") and the applyXL("query") function. The main limitation of this approach is that the applyXL function can only be used to read the graph not to manipulate it.  

```java
public void applyRGGfunction(){
 SecGraphRef sgr = new SecGraphRef("one");
 //apply command on the graph similar to the XL console
 sgr.applyXL("((*Node*))");
 // execute RGG functions on the SecGraph
 sgr.apply("rggFunction");
 sgr.applyXL("((*Node*))");
}

/**
* example function for execution
*/
public void rggFunction()[
 M ==> M RL(90) M;
]

```
This can be seen in the example: *examples:Graph Explorer/SecGraphCreationExample* 


### Auto-recreate

If a secondary graph is constructed using Modules defined in the RGG code, the graph reconstruction will fail after the code is recompiled because the graph is looking for the old classes. Therefore the option autoRecreate can be used with `sg.setAutoRecreate(true);`. 
This can be seen in the example: *examples:Graph Explorer/SecGraphSaveAndLoadExample*
