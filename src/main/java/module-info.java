module graph.explorer {
	exports de.grogra.graph.object;
	exports de.grogra.graph.object.impl;
	exports de.grogra.graph.object.impl.io;
	exports de.grogra.graph.object.sg;
	exports de.grogra.graph.object.sg.impl;
	exports de.grogra.graph.object.sg.impl.io;

	requires platform.core;
	requires platform;
	requires utilities;
	requires graph;
	requires xl.core;
	requires xl;
	requires xl.compiler;
	requires imp3d;
	requires imp2d;
	requires imp;
	requires rgg;
	requires vecmath;
	requires java.xml;
	requires xl.vmx;
	requires java.desktop;


}
