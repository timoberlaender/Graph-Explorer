package de.grogra.graph.object;

import de.grogra.graph.Graph;

public interface GraphContext {
	public Graph getGraph();
}
