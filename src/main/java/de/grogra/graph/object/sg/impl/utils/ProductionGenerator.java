package de.grogra.graph.object.sg.impl.utils;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.rgg.model.RGGProducer;
import de.grogra.xl.query.EdgeDirection;
import de.grogra.xl.util.ObjectList;

import java.util.HashMap;

public class ProductionGenerator {
	private boolean isRegisterd(Node n) {
		return nodeMap.keySet().contains(n.getId());
	}
	HashMap<Long,Node> nodeMap;
	RGGProducer rgp;
	public ProductionGenerator(RGGProducer rgp) {
		this.rgp=rgp;
		nodeMap = new HashMap<Long,Node>();
		
	}
	
	public void generate(Node root) throws CloneNotSupportedException {
		if(root!=null) {
			registerNode(root);
			load(root);
		}
	}	
	
	private void load(Node node) throws CloneNotSupportedException {
		ObjectList<Node> successors= new ObjectList<Node>();
		rgp.producer$push();
		for(Edge e = node.getFirstEdge();e!=null;e = e.getNext(node)) {
			if(e.isSource(node)) {
				Node kid = e.getTarget();
				if(isRegisterd(kid)) {
					registerEdge(e.getEdgeBits(),nodeMap.get(node.getId()),nodeMap.get(kid.getId()));

				}else {
					if(e.getEdgeBits() == Graph.SUCCESSOR_EDGE) {
						successors.add(e.getTarget());
					}else {
						
						registerNode(kid);
						registerEdge(e.getEdgeBits(),nodeMap.get(node.getId()),nodeMap.get(kid.getId()));
						
						load(kid);
					}
				}
			}
		}
		rgp.producer$pop(null);
		for(Node n : successors) {
			registerNode(n);
			registerEdge(Graph.SUCCESSOR_EDGE,nodeMap.get(node.getId()),nodeMap.get(n.getId()));
			load(n);
		}
	}

	private void registerEdge(int edgeBits, Node in, Node out) {
		rgp.addEdgeImpl(in, out, edgeBits, EdgeDirection.FORWARD);
		
	}

	private void registerNode(Node kid) throws CloneNotSupportedException {
		Node copy = kid.clone(true);
		nodeMap.put(kid.getId(), copy);
		rgp.addNodeImpl(copy, false);
	}
}
