package de.grogra.graph.object.sg.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemVisitor;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.TypeItem;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.reflect.Field;
import de.grogra.reflect.Member;
import de.grogra.reflect.Method;
import de.grogra.reflect.Reflection;
import de.grogra.reflect.Type;
import de.grogra.rgg.Library;
import de.grogra.rgg.model.CompilationFilter;
import de.grogra.rgg.model.Runtime;
import de.grogra.rgg.model.ShellFilter;
import de.grogra.rgg.model.XLFilter;
import de.grogra.util.DelegatingClassLoader;
import de.grogra.util.DetailedException;
import de.grogra.util.Utils;
import de.grogra.xl.compiler.CClass;
import de.grogra.xl.compiler.scope.ClassPath;
import de.grogra.xl.compiler.scope.CompilationUnitScope;
import de.grogra.xl.lang.DisposableIterator;

public class XLinLineRunner implements ItemVisitor
	{
	public static final String SHELL_PACKAGE = "$shell$";
	public static final String SHELL_CLASS = "Shell";

		static final String PROMPT = "> ";

		ClassPath path = new ClassPath (XLFilter.getLoaderForAll ());
		CClass shell;
		ArrayList<ClassLoader> loaders = new ArrayList<ClassLoader> ();

		XLinLineRunner ()
		{
			shell = new CClass (SHELL_CLASS,
				SHELL_PACKAGE + '.' + SHELL_CLASS, Member.PUBLIC | Member.ABSTRACT,
				null, true);
			shell.initSupertype (Type.OBJECT);
			shell.initTypeLoader (path);
		}
		
		public void visit (Item item, Object info)
		{
			if (item instanceof TypeItem)
			{
				Type t = (Type) ((TypeItem) item).getObject ();
				if (t.getDeclaringType () == null)
				{
					path.getPackage (t.getPackage (), true).declareType (t);
					Class cls = t.getImplementationClass ();
					if (!Reflection.canLoad (path.getClassLoader (), cls))
					{
						ClassLoader x = cls.getClassLoader ();
						for (int i = loaders.size () - 1; i >= 0; i--)
						{
							if (Reflection.canLoad (loaders.get (i), cls))
							{
								return;
							}
						}
						for (int i = loaders.size () - 1; i >= 0; i--)
						{
							if (Reflection.isAncestorOrSame (loaders
								.get (i), x))
							{
								loaders.remove (i);
							}
						}
						loaders.add (x);
					}
				}
			}
		}

		public String getCommandName ()
		{
			return "xxx";
		}

		private CompilationUnitScope lastScope;
		private Type lastShell;

		public void run (String cmd ,Registry reg)
		{
			Throwable ex = null;
			if ((shell != null) && (cmd.trim ().length () > 0))
			{
				for (int i = shell.getDeclaredMethodCount () - 1; i >= 0; i--)
				{
					shell.removeMethod (shell.getDeclaredMethod (i));
				}
				loaders.clear ();
				Item
					.forAll (reg, "/classes", null, null, this, null, false);
				ClassLoader cl = path.getClassLoader ();
				if (!loaders.isEmpty ())
				{
					loaders.add (cl);
					cl = new DelegatingClassLoader (loaders
						.toArray (new ClassLoader[loaders.size ()]));
				}
				HashSet<String> removedFields = new HashSet<String> ();
				HashSet<Field> oldFields = new HashSet<Field> ();
				for (int i = shell.getDeclaredFieldCount () - 1; i >= 0; i--)
				{
					Field x = shell.getDeclaredField (i);
					if (!(Reflection.isPrimitive (x.getType ()) || Reflection
						.canLoad (cl, x.getType ()
							.getImplementationClass ())))
					{
						shell.removeField (x);
						removedFields.add (x.getDescriptor ());
					}
					else
					{
						oldFields.add (x);
					}
				}
				CompilationFilter f;
				Type compiledShell;
				try
				{
					if (cmd.charAt (cmd.length () - 1) != ';')
					{
						cmd += "\n;";
					}
					f = new CompilationFilter (null, new ShellFilter (reg,
						path, cmd, lastScope));
					compiledShell = f.compile (shell, cl)[0];
				}
				catch (IOException e)
				{
					for (int i = shell.getDeclaredFieldCount () - 1; i >= 0; i--)
					{
						Field x = shell.getDeclaredField (i);
						if (!oldFields.contains (x))
						{
							shell.removeField (x);
						}
					}
					f = null;
					compiledShell = null;
					Throwable t = Utils.getMainException (e);
					if (t instanceof DetailedException)
					{
						System.err.println (
							((DetailedException) t)
								.getDetailedMessage (false));
					}
					else
					{
						ex = t;
					}
				}
				if (f != null)
				{
					lastScope = f.getCompilationUnit ();
					if (lastShell != null)
					{
						for (int i = lastShell.getDeclaredFieldCount () - 1; i >= 0; i--)
						{
							Field x = lastShell.getDeclaredField (i);
							if ((x.getModifiers () & (Member.PUBLIC | Member.SYNTHETIC))
								== Member.PUBLIC)
							{
								Field y = Reflection.getDeclaredField (
									compiledShell, x.getDescriptor ());
								if ((y != null) && !removedFields.contains (x.getDescriptor ()))
								{
									try
									{
										Object value = Reflection.get (null, x);
										Reflection.set (null, y, value);
									}
									catch (Exception e)
									{
										System.err.println (x.getDescriptor () + " " + e);
									}
								}
							}
						}
					}
					lastShell = compiledShell;
					Method m = Reflection.getDeclaredMethod (compiledShell,
						"execute");
					if (m != null)
					{
						try
						{
							if (Runtime.INSTANCE.currentGraph () != null)
							{
								DisposableIterator i = Library.apply (1);
								try
								{
									while (i.next ())
									{
										m.invoke (null, null);
									}
									i.dispose (null);
								}
								catch (Throwable e)
								{
									i.dispose (e);
									Utils.rethrow (e);
								}
							}
							else
							{
								m.invoke (null, null);
							}
						}
						catch (Exception e)
						{
							ex = e;
						}
					}
				}
			}
			if (ex != null)
			{
				System.err.println (ex);
			}
			Utils.rethrow (ex);
		}

	}