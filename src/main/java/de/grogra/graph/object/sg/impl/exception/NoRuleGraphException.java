package de.grogra.graph.object.sg.impl.exception;

public class NoRuleGraphException extends Exception{

	public NoRuleGraphException(String message) {
		super(message);
	}
	public NoRuleGraphException() {
		super("The current SecGraph does not include a Query- and a Production-Root");
	}
	
}
