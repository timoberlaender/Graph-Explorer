package de.grogra.graph.object.sg.impl;

import de.grogra.graph.impl.Node;
import de.grogra.graph.object.sg.SecGraph;
import de.grogra.graph.object.sg.impl.exception.NoQueryGraphException;
import de.grogra.graph.object.sg.impl.exception.NoRuleGraphException;
import de.grogra.graph.object.sg.impl.utils.PatternGenerator;
import de.grogra.graph.object.sg.impl.utils.ProductionGenerator;
import de.grogra.pf.ui.Workbench;
import de.grogra.rgg.model.RGGGraph;
import de.grogra.rgg.model.RGGProducer;
import de.grogra.rgg.model.Runtime;
import de.grogra.xl.query.CompoundPattern;
import de.grogra.xl.query.Query;
import de.grogra.xl.vmx.Authorization;
import de.grogra.xl.vmx.RoutineBase;
import de.grogra.xl.vmx.RoutineDescriptor;
import de.grogra.xl.vmx.VMXState;
import de.grogra.xl.vmx.AbruptCompletion.Return;

public class Library {
	
	
	public static SecGraph operator$add(SecGraph s1,SecGraph s2) {
		SecGraphImpl x = new SecGraphImpl(Workbench.current(),s1);
		x.add(s2);
		return x;
	}
	
	public static SecGraphRef secgraph(String name) {
		return new SecGraphRef(name);
	}
	
/*	public static SecGraph operator$sub(SecGraph s1,SecGraph s2) throws CloneNotSupportedException, NoRuleGraphException {
		SecGraphImpl x = new SecGraphImpl(Workbench.current(),s1);
		SecGraphImpl rule = new SecGraphImpl(Workbench.current());
		rule.getQueryRoot(true).addEdgeBitsTo(s2.cloneGraph().getTarget(), Graph.SUCCESSOR_EDGE, null);
		rule.getProductionRoot(true);
		apply(rule,x);
		return x;
	}
	*/
	
	/**
	 * Apply a <code> rule</code> (a SecGraph with a queryRoot and a ProductionRoot) on the ProjectGraph
	 * @param rule RuleGraph
	 * @throws NoRuleGraphException
	 */
	public static void apply(SecGraph rule) throws NoRuleGraphException {
		RGGGraph gr = Runtime.INSTANCE.currentGraph();
		apply(rule, gr);
	}
	/**
	 * Apply a <code> rule</code> (a SecGraph with a queryRoot and a ProductionRoot) on the Destination-SecGraph. By setting the {@code in} to be current.
	 * @param rule RuleGraph
	 * @param on Destination-SecGraph
	 * @throws NoRuleGraphException
	 */
	
	public static void apply(SecGraph rule, SecGraph on) throws NoRuleGraphException {
		on.setCurrent();
		RGGGraph gr = Runtime.INSTANCE.currentGraph();
		apply(rule, gr);
		on.releaseCurrent();
	}
	/**
	 * Apply a <code> rule</code> (a SecGraph with a queryRoot and a ProductionRoot) on the given RGGGraph
	 * @param rule RuleGraph 
	 * @param gr RGGGraph
	 * @throws NoRuleGraphException
	 */
	
	 static void apply(SecGraph rule, RGGGraph gr) throws NoRuleGraphException {
		if(!rule.isRuleGraph() || rule.getQueryRoot(false).getFirst(de.grogra.graph.Graph.SUCCESSOR_EDGE)==null) {
			throw new NoRuleGraphException();
		}
		
		PatternGenerator cpg = new PatternGenerator();
		CompoundPattern cp = cpg.generate(rule.getQueryRoot(false).getFirst(de.grogra.graph.Graph.SUCCESSOR_EDGE));

		
//		CompoundPatternGenerator cpg = new CompoundPatternGenerator();
//		CompoundPattern cp = cpg.create(rule.getQueryRoot(false).getFirst(de.grogra.graph.Graph.SUCCESSOR_EDGE));

		// creating a new VMX Frame to store query data
		VMXState vmx = VMXState.current();
		Authorization auth = new Authorization();
		VMXState.Local vqs = new VMXState.Local(0, 0);
		vmx.enter(16, auth);

		// creating a query
		boolean forProduction = true; // defines if the results are nodes or producers
		VMXState.Local[] vars = new VMXState.Local[cpg.getVarLength()]; // an array of empty locals (the size of the
		Query q = new Query(cp, forProduction, vqs, vars);
		//
		RoutineBase r = new RoutineBase(0, false, 1, 0, 1) {

			@Override
			public Return execute(VMXState s) {
					
				if(rule.getProductionRoot(false).getFirst(de.grogra.graph.Graph.SUCCESSOR_EDGE)!=null) {
					RGGProducer rp = (RGGProducer) s.aget(vqs, auth);
					rp.producer$beginExecution(RGGProducer.SIMPLE_ARROW);
				
					ProductionGenerator pg = new ProductionGenerator(rp);
					try {
						pg.generate(rule.getProductionRoot(false).getFirst(de.grogra.graph.Graph.SUCCESSOR_EDGE));
					} catch (CloneNotSupportedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					rp.producer$endExecution(true);

				}
				
				return s.newReturn();
			}
		};

		RoutineDescriptor rd = vmx.createDescriptor(r, -1, auth);
		q.findObjectMatches(rd, vmx.getFrame(auth), gr.createQueryState());
	}

	static int count = 0;

	/**
	 * Tests if the given SecGraph can be found in the Project Graph
	 * @param query SecGraph
	 * @return
	 * @throws NoQueryGraphException
	 */
	
	public static boolean inProjectGraph(SecGraph query) throws NoQueryGraphException {
		return inGraph(query, false);
	}

	/**
	 * Tests if either full SecGraph or the query subgraph of it can be found in the Project Graph
	 * @param query SecGraph
	 * @param asQuery asQuery
	 * @return
	 * @throws NoQueryGraphException
	 */
	
	public static boolean inProjectGraph(SecGraph query, boolean asQuery) throws NoQueryGraphException {
		return inGraph(query, asQuery);
	}

	/**
	 * Tests if the given query-SecGraph can be found in a second secGraph(destination)
	 * @param query SecGraph
	 * @param in Destination-SecGraph
	 * @return
	 * @throws NoQueryGraphException
	 */
	
	public static boolean includedIn(SecGraph query, SecGraph in) throws NoQueryGraphException {
		return includedIn(query,in,false);
	}

	/**
	 * Tests if the given SecGrph or the query subgraph can be found in the proveided Destination-SecGraph. By setting the Destination-SecGraph to be current.
	 * @param query SecGraph
	 * @param in Destination-SecGraph
	 * @param asQuery
	 * @return
	 * @throws NoQueryGraphException
	 */
	
	public static boolean includedIn(SecGraph query, SecGraph in, boolean asQuery) throws NoQueryGraphException {
		in.setCurrent();
		boolean isin = inGraph(query, asQuery);
		in.releaseCurrent();
		return isin;
	}

	/**
	 * Tests if the given SecGraph or the query subgraph can be found in the currently used RGGGraph.
	 * @param sq SecGraph
	 * @param asQuery
	 * @return
	 * @throws NoQueryGraphException
	 */
	
	
	public static boolean inGraph(SecGraph sg, boolean asQuery) throws NoQueryGraphException {
		if (asQuery) {
			if(!sg.isQueryGraph() || sg.getQueryRoot(false).getFirst(de.grogra.graph.Graph.SUCCESSOR_EDGE)==null) {
				throw new NoQueryGraphException();
			}
			return inGraph(sg.getQueryRoot(false).getFirst(de.grogra.graph.Graph.SUCCESSOR_EDGE));
		} else {
			if(sg.getRootNode().getFirstEdge().getTarget()==null) {
				return true; // an empty graph is included -,-'
			}
			return inGraph(sg.getRootNode().getFirstEdge().getTarget());

		}
	}

	/**
	 * Tests if the subgraph below the RootNode can be found in the currently used RGGgraph
	 * @param  start RootNode
	 * @return
	 * @throws NoQueryGraphException
	 */
	
	public static boolean inGraph(Node start) {
		RGGGraph gr = Runtime.INSTANCE.currentGraph();
		count = 0;
		PatternGenerator cpg = new PatternGenerator();
		CompoundPattern cp = cpg.generate(start);
		// creating a new VMX Frame to store query data
		VMXState vmx = VMXState.current();
		Authorization auth = new Authorization();
		VMXState.Local vqs = new VMXState.Local(0, 0);
		vmx.enter(16, auth);

		// creating a query
		boolean forProduction = false; // defines if the results are nodes or producers
		VMXState.Local[] vars = new VMXState.Local[cpg.getVarLength()]; // an array of empty locals (the size of the
		Query q = new Query(cp, forProduction, vqs, vars);
		//
		RoutineBase r = new RoutineBase(0, false, 1, 0, 1) {
			@Override
			public Return execute(VMXState s) {
				count++;
				return s.newReturn();
			}
		};

		RoutineDescriptor rd = vmx.createDescriptor(r, -1, auth);
		q.findObjectMatches(rd, vmx.getFrame(auth), gr.createQueryState());
		return (count > 0);
	}
}
