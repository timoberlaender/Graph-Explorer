package de.grogra.graph.object.sg.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.Executor;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.GraphContext;
import de.grogra.graph.object.impl.GraphInstance;
import de.grogra.graph.object.sg.SecGraph;
import de.grogra.graph.object.sg.impl.exception.NoQueryGraphException;
import de.grogra.graph.object.sg.impl.exception.NoRuleGraphException;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemReference;
import de.grogra.pf.registry.ObjectItem;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.ProjectFileObjectItem;
import de.grogra.rgg.model.Compiletime;
import de.grogra.rgg.model.RGGGraph;
import de.grogra.vfs.FileSystem;
import de.grogra.xl.lang.ObjectConsumer;
import de.grogra.xl.query.EdgeDirection;
import de.grogra.xl.query.HasModel;
import de.grogra.xl.query.MatchConsumer;
import de.grogra.xl.query.Pattern;
import de.grogra.xl.query.Pattern.Matcher;
import de.grogra.xl.query.Producer;
import de.grogra.xl.query.QueryState;
import de.grogra.xl.query.RuntimeModel;
import de.grogra.xl.util.IntList;
import de.grogra.xl.util.XBitSet;

@HasModel(Compiletime.class)
public final class SecGraphRef extends ItemReference<SecGraph> implements SecGraph {

	//enh:sco
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends ItemReference.Type
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (SecGraphRef representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, ItemReference.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new SecGraphRef ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (SecGraphRef.class);
		$TYPE.validate ();
	}

//enh:end

	SecGraphRef() {
		super(null);
	}
	
	public SecGraphRef(String name) {
		super(name);
	}
	
	public SecGraphRef(String name, Node root) {
		super(name);
		Node r = new Node();
		setRootNode(r);
		try {
			r.addEdgeBitsTo(root.cloneGraph(EdgePatternImpl.FORWARD, true), Graph.BRANCH_EDGE, null);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public SecGraphRef(String name, GraphContext inp) {
		super(name);
		for(String key : inp.getGraph().getRootKeys()) {
			Object r = inp.getGraph().getRoot(key);
			if(r instanceof Node) {
				Node n;
				try {
					n = ((Node)r).cloneGraph(EdgePatternImpl.FORWARD, true);
					resolve().getGraph().setRoot(key,n);
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public static SecGraphRef get(String name) {
		return new SecGraphRef(name);
	}
	
	
	public synchronized SecGraph resolve() {
		if(objectResolved ) {
			return object;
		}
		return resolveObject ("/objects/secgraphs", Registry.current ());
	}

	public SecGraph toSecGraph() {
		return resolve();
	}
	
	
	public static SecGraph all(ObjectConsumer consumer) {
		Item projectDir = Item.resolveItem(Workbench.current().getRegistry(), "/project/objects/secgraphs");
		for (Node n = projectDir.getBranch(); n != null; n = n.getSuccessor()) {
			if (n instanceof ProjectFileObjectItem) {
				Object o = ((ProjectFileObjectItem) n).getObject();
				if (o != null && o instanceof SecGraph) {
					consumer.consume(((SecGraph) o));
				} else {
					consumer.consume(((SecGraph) n));
				}
			}
			if (n instanceof SecGraph) {
				consumer.consume(((SecGraph) n));
			}
		}
		return null;
	}

	
	
	@Override
	protected Item createItem (RegistryContext ctx, String directory, String name)
	{
		// check if groimp has a filter that support writing graph into virtual files
		
		Object sg = new SecGraphImpl(Workbench.current());
//		Item i = new SharedValue (name, sg);
		FileSystem pfs = ctx.getRegistry().getFileSystem();
		Object dest =null;
		Object dir = pfs.getRoot();
		try {
			dir = pfs.create(dir, "secgraphs", true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			dest = pfs.createIfDoesntExist(dir, name, false, false);
//			pfs.addLocalFile(null, dir, pfs.getName(dest), false, false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ObjectItem i = new ProjectFileObjectItem(IO.toSystemId(pfs, dest),  GraphXMLSource.MIME_TYPE, sg,
				SecGraphImpl.class.getName(), SecGraphImpl.MIME_TYPE);
		
		ctx.getRegistry ().getDirectory (directory, null).add (i);
		i.makeUserItem (false);
		return i;
	}

	public void setRootNode (Node n) {
		resolve().setRootNode(n);
	}
	
	public Node getRootNode () {
		return resolve().getRootNode();
	}
	
	public Node cloneGraph() throws CloneNotSupportedException{
		return resolve().cloneGraph();
	}
	

	public void write() {
		resolve ().write();
	}
	
	public void reload() {
		
		resolve ().reload();
	}

	public void clear() {
		resolve().clear();
	}
	
	public Object describes() {
		return resolve ().describes();
	}

	
	
	public Object clone() throws CloneNotSupportedException {
		return resolve().clone();

	}

	
	@Override
	public Registry getRegistry() {
		return resolve ().getRegistry();
	}

	@Override
	public GraphManager getGraph() {
		return resolve ().getGraph();
	}
	
	public Node getNode(String key) {
		return resolve ().getNode(key);
	}

	@Override
	public void apply(String name) {
		resolve().apply(name);
		
	}
	@Override
	public void applyXL(String name) {
		resolve().applyXL(name);
		
	}

	public RGGGraph getRGGGraph() {
		return resolve().getRGGGraph();
	}

	
	public Node toInstance() {
		if(getRootNode()==null) {
			return new Node();
		}
		return new GraphInstance(this);
	}
	public Node toNode() {
		return toInstance();
	}
	public void setAutoRecreate(boolean b) {
		resolve().setAutoRecreate(b);
	}
	@Override
	public void handleSaveEvent() {
		resolve().handleSaveEvent();
		
	}
	//initialize the producer to with the ==> command 
	public SecGraphInitialProducer producer$begin(){
		
		if (getGraph().getMainState().getContext() != Workbench.current().getJobManager().getThreadContext()) {
			getGraph().initMainState((Executor) Workbench.current().getJobManager());
		}
		resolve().clear();
		return (SecGraphInitialProducer)getInitalProducer();
	}

	@Override
	public Object getInitalProducer() {
		return resolve().getInitalProducer();
	}

	@Override
	public void setCurrent() {
		resolve().setCurrent();
		
	}

	@Override
	public void releaseCurrent() {
		resolve().releaseCurrent();
	}



	@Override
	public boolean includes(SecGraph sg) throws NoQueryGraphException {
		return resolve().includes(sg);
	}

	@Override
	public boolean inProjectGraph() throws NoQueryGraphException {
		return resolve().inProjectGraph();
	}
	
	@Override
	public boolean includes(SecGraph sg, boolean asQuery) throws NoQueryGraphException {
		return resolve().includes(sg, asQuery);
	}

	@Override
	public boolean inProjectGraph(boolean asQuery) throws NoQueryGraphException {
		return resolve().inProjectGraph(asQuery);
	}

	@Override
	public void execute() throws NoRuleGraphException {
		resolve().execute();
	}

	@Override
	public void execute(SecGraph sg) throws NoRuleGraphException {
		resolve().execute(sg);
		
	}

	@Override
	public void apply(SecGraph rule) throws NoRuleGraphException {
		resolve().apply(rule);
	}

	@Override
	public Node getQueryRoot(boolean create) {
		return resolve().getQueryRoot(create);
	}

	@Override
	public Node getProductionRoot(boolean create) {
		return resolve().getProductionRoot(create);
	}

	@Override
	public SecGraphInitialProducer query() {
		return (SecGraphInitialProducer) resolve().query();
	}

	@Override
	public SecGraphInitialProducer production() {
		return (SecGraphInitialProducer) resolve().production();
	}

	@Override
	public boolean isQueryGraph() {
		return resolve().isQueryGraph();
	}

	@Override
	public boolean isRuleGraph() {
		return resolve().isRuleGraph();
	}


	@Override
	public Node produceNode() {
		return toInstance();
	}

	@Override
	public void view3d(Context ctx) {
		resolve().view3d(ctx);
		
	}

	@Override
	public void view2d(Context ctx) {
		resolve().view2d(ctx);
	}
	
	@Override
	public void add(GraphContext imp) {
		resolve().add(imp);
	}

	@Override
	public RuntimeModel getModel() {
		return resolve().getModel();
	}

	@Override
	public QueryState createQueryState() {
		return resolve().createQueryState();
	}

	@Override
	public Producer createProducer(QueryState qs) {
		return resolve().createProducer(qs);	
	}

	@Override
	public Matcher createMatcher(Pattern pred, XBitSet providedConstants, IntList neededConstantsOut) {
		return resolve().createMatcher(pred, providedConstants, neededConstantsOut);
	}

	@Override
	public boolean canEnumerateNodes(de.grogra.reflect.Type type) {
		return resolve().canEnumerateNodes(type);
	}

	@Override
	public void enumerateNodes(de.grogra.reflect.Type type, QueryState qs, int tp, MatchConsumer consumer, int arg) {
		resolve().enumerateNodes(type, qs, tp, consumer, arg);
	}

	@Override
	public boolean canEnumerateEdges(EdgeDirection dir, boolean constEdge, Serializable edge) {
		return resolve().canEnumerateEdges(dir, constEdge, edge);
	}

	@Override
	public void enumerateEdges(Object node, EdgeDirection dir, de.grogra.reflect.Type edgeType, QueryState qs,
			int toIndex, int patternIndex, Serializable pattern, int matchIndex, MatchConsumer consumer, int arg) {
		resolve().enumerateEdges(node, dir, edgeType, qs, toIndex, patternIndex, pattern, matchIndex, consumer, arg);
	}

	@Override
	public Object getRoot() {
		return resolve().getRoot();
	}

	@Override
	public Object getTypeRoot() {
		return resolve().getTypeRoot();
	}

	@Override
	public void enumerateSpaces(Object node, EdgeDirection dir, de.grogra.reflect.Type edgeType, QueryState qs,
			int toIndex, int patternIndex, Serializable pattern, int matchIndex, MatchConsumer consumer, int arg) {
		resolve().enumerateSpaces(node, dir, edgeType, qs, toIndex, patternIndex, pattern, matchIndex, consumer, arg);
	}

	@Override
	public HashMap<Object, Integer> sortedTypeGraph() {
		return resolve().sortedTypeGraph();
	}

	@Override
	public long derive() {
		return resolve().derive();
	}
}