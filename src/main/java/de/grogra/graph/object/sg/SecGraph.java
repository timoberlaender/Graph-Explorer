package de.grogra.graph.object.sg;

import de.grogra.graph.impl.Node;
import de.grogra.graph.object.GraphContext;
import de.grogra.graph.object.GraphObject;
import de.grogra.graph.object.sg.impl.SecGraphInitialProducer;
import de.grogra.graph.object.sg.impl.exception.NoQueryGraphException;
import de.grogra.graph.object.sg.impl.exception.NoRuleGraphException;
import de.grogra.rgg.model.Compiletime;
import de.grogra.rgg.model.RGGGraph;
import de.grogra.xl.query.HasModel;

/**
 * defines what a graph object stored in the SecGraph explorer should be able to do
 */

@HasModel(Compiletime.class)
public interface SecGraph extends GraphObject, Cloneable, de.grogra.xl.query.Graph {


	final String QUERY_ROOT = "QueryRoot";

	final String PRODUCTION_ROOT = "ProductionRoot";

	
		
	/**
	 * removes all Nodes except the root
	 */
	public void clear();
	
	
	/**
	 * Gives an array of node types and number contained in the SecGraph
	 * @return
	 */
	/**
	 * Apply a RGG function on the SecGraph
	 * 
	 * @param name: name of the function
	 */
	
	public void apply(String name);
	
	/**
	 * Apply an XL Query on the SecGraph
	 * @param query: the XL query
	 */
	
	public void applyXL(String query);
	
	
	/**
	 * creates a Node holding a instanciation of the SecGraph
	 * @return the Node
	 */
	
	public Node toInstance();
	
	/**
	 * function triggered by the saving of the project
	 */
	public void handleSaveEvent();
	
	/**
	 * defines if the SecGraph is automatically recreated if the model is saved
	 * @param b sets if recreated or not
	 */
	
	public void setAutoRecreate(boolean b);
	

	/**
	 * create a new producer to initialize the Graph
	 * @return
	 */
	
	public Object getInitalProducer();
	
	/**
	 * set this graph to be the currently used one
	 * 
	 */
	
	public void setCurrent();
	
	/**
	 * set the project graph to be teh current one
	 * 
	 */
	
	public void releaseCurrent();

	/**
	 * test if this graph is includes an other graph
	 */
		
	/**
	 * check if the secgraph includes a queryRoot
	 * @return
	 */
	public boolean isQueryGraph();
	/**
	 * check if the secgraph includes a QueryRoot and a ProductionRoot
	 * @return
	 */
	
	public boolean isRuleGraph();
	
	
	public Node getQueryRoot(boolean create);
	
	public Node getProductionRoot(boolean create);
	
	public Object query();
	
	public Object production();
	
	/***
	 * test if this SecGraph incldues the SecGraph SG
	 * @param sg
	 * @return
	 * @throws NoQueryGraphException 
	 */
	
	
	
	public boolean includes(SecGraph sg) throws NoQueryGraphException;

	
	/***
	 * test if this SecGraph  incldues the given SecGraph   or if the query SecGraph  of this graph is included in the given SecGraph
	 * @param sg
	 * @return
	 * @throws NoQueryGraphException 
	 */
	
	public boolean includes(SecGraph sg, boolean asQuery) throws NoQueryGraphException;

	
	/**
	 * test if this graph is included in the ProjectGraph
	 * @return
	 * @throws NoQueryGraphException 
	 */
	
	public boolean inProjectGraph() throws NoQueryGraphException;
	
	
	/**
	 * test if this graph is included in the ProjectGraph or if the query SecGraph of this graph is included in the Project Graph
	 * @param asQuery
	 * @return
	 * @throws NoQueryGraphException 
	 */
	
	public boolean inProjectGraph(boolean asQuery) throws NoQueryGraphException;
	
	/**
	 * run this SecGraph as a rule on the MainGraph
	 * @throws NoRuleGraphException 
	 */
	
	public void execute() throws NoRuleGraphException;

	/**
	 * run this SecGraph as a rule on the given secGraph 
 	 * @param sg
	 * @throws NoRuleGraphException 
	 */
	
	public void execute(SecGraph sg) throws NoRuleGraphException;

	
	/**
	 * apply the given SecGraph on this SecGraph
	 * @param rule
	 * @throws NoRuleGraphException 
	 */
	public void apply(SecGraph rule) throws NoRuleGraphException;
	
	
	public Object clone() throws CloneNotSupportedException;	 
	
	
	public void add(GraphContext sg);

	public SecGraphInitialProducer producer$begin();

	public RGGGraph getRGGGraph();

	
	public long derive ();
}





