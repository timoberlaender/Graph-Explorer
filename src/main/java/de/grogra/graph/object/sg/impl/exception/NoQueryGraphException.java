package de.grogra.graph.object.sg.impl.exception;

public class NoQueryGraphException extends Exception{
	private static final long serialVersionUID = 1L;
	public NoQueryGraphException(String message) {
		super(message);
	}
	public NoQueryGraphException() {
		super("The used SecGraph does not contain a QueryRoot");
	}
}
