package de.grogra.graph.object.sg.impl.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

import de.grogra.graph.object.sg.SecGraph;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.StreamAdapter;
import de.grogra.pf.io.VirtualFileWriterSource;
import de.grogra.pf.io.WriterSource;
import de.grogra.vfs.FileSystem;

public class SecGraphExporter extends FilterBase implements  FileWriterSource, VirtualFileWriterSource,
WriterSource {

	public SecGraphExporter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(item.getOutputFlavor()); 
	}


	private StreamAdapter getStreamAdapter() throws IOException {
		if(source instanceof ObjectSource) {
			Object o = ((ObjectSource) source).getObject();
			if(o instanceof SecGraph) {
				GraphXMLSource gxs = new GraphXMLSource(((SecGraph) o).getGraph(), getRegistry(), null);
				StreamAdapter sad = new StreamAdapter(gxs,
						new IOFlavor(GraphXMLSource.MIME_TYPE, IOFlavor.OUTPUT_STREAM, null));
				return sad;
			}
		}
		return null;
		
		
	}
	@Override
	public void write(Writer out) throws IOException {
		System.err.println("whaaaats up");
	}

	@Override
	public void write(FileSystem fs, Object out) throws IOException {
		Object file =fs.getFile(IO.toPath(getSystemId()));
		getStreamAdapter().write(fs,file);

		
	}

	@Override
	public void write(File out) throws IOException {
		getStreamAdapter().write(out);
		
	}

}
