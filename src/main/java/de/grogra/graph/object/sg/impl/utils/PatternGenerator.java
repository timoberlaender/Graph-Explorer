package de.grogra.graph.object.sg.impl.utils;

import java.util.HashMap;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.reflect.Type;
import de.grogra.xl.query.CompoundPattern;
import de.grogra.xl.query.EdgePattern;
import de.grogra.xl.query.Pattern;
import de.grogra.xl.query.TypePattern;
import de.grogra.xl.util.ObjectList;

public class PatternGenerator {

	Node mds; // most distance Sucessor;
	ObjectList<Node> nodeStack;
	ObjectList<Edge> edgeStack;

	public PatternGenerator() {
		nodeStack = new ObjectList<Node>();
		edgeStack = new ObjectList<Edge>();
	}

	public CompoundPattern generate(Node root) {
		mds = root;
//		nodeStack.add(root);
		load(root);

		int length = nodeStack.size + edgeStack.size + 1;
		Type[] types = new Type[length];
		boolean[] paramIsNode = new boolean[length];
		Pattern[] predicates = new Pattern[length];
		short[][] paramMappings = new short[length][];
		boolean[] forceParamIsContext = new boolean[length];
		boolean[] predIsContext = new boolean[length];
		short[][] dependencies = new short[length][];
		short[][] foldings = new short[length][];
		HashMap<Long, Short> nodeMap = new HashMap<Long, Short>();
		nodeMap.put(mds.getId(), (short)(length-1));
		types[length-1]=mds.getNType();
		paramIsNode[length-1]=true;
		predicates[length-1] = new TypePattern(mds.getNType());
		paramMappings[length-1] = new short[] { (short)(length-1) };
		forceParamIsContext[length-1] = false;
		predIsContext[length-1] = false;
		dependencies[length-1] = new short[] {};
		foldings[length-1] = new short[] {};
		
		
		int i = 0;
		for (i=0; i < nodeStack.size; i++) {
			Node x = nodeStack.get(i);
			nodeMap.put(x.getId(), (short) i);
			types[i] = x.getNType();
			paramIsNode[i] = true;
			predicates[i] = new TypePattern(x.getNType());
			paramMappings[i] = new short[] { (short) i };
			forceParamIsContext[i] = false;
			predIsContext[i] = false;
			dependencies[i] = new short[] {};
			foldings[i] = new short[] {};
		}
		
		for(int j=0; j<edgeStack.size; j++) {
			Edge e = edgeStack.get(j);
			types[i+j]=Type.INT;
			paramIsNode[i+j]=false;
			predicates[i+j]=new EdgePattern(Node.$TYPE,de.grogra.reflect.Type.INT,e.getEdgeBits(),0);
			paramMappings[i+j] = new short[] {nodeMap.get(e.getSource().getId()),nodeMap.get(e.getTarget().getId()) ,(short) (i+j) };
			forceParamIsContext[i+j] = false;
			predIsContext[i+j] = false;
			dependencies[i+j] = new short[] {};
			foldings[i+j] = new short[] {};
		}
		int inp =nodeMap.get(root.getId());
		int out=nodeMap.get(mds.getId());
		int direction=2;
		int predType=0;
		boolean optional = false;
		String continueLabel = null;
		CompoundPattern cp = new CompoundPattern(types,paramIsNode,forceParamIsContext,predicates,predIsContext,paramMappings,dependencies,foldings,inp,out,direction,predType,optional,null);
		return cp;
		

	}

	public void load(Node node) {
		for (Edge e = node.getFirstEdge(); e != null; e = e.getNext(node)) {
			if (e.isSource(node)) {
				edgeStack.add(e);
				if (e.getEdgeBits() == Graph.SUCCESSOR_EDGE && node == mds) {
					nodeStack.add(mds);
					mds = e.getTarget();
				} else {
					nodeStack.add(e.getTarget());
				}
				load(e.getTarget());
			}
		}
	}
	

	public int getVarLength() {
		return  nodeStack.size + edgeStack.size + 1;
	}
}
