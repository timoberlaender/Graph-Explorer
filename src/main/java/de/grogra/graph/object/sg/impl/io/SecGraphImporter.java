package de.grogra.graph.object.sg.impl.io;

import java.io.File;
import java.io.IOException;

import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.StreamAdapter;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.ProjectFileObjectItem;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.vfs.FileSystem;
import de.grogra.xl.util.ObjectList;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.sg.impl.SecGraphImpl;
import de.grogra.pf.registry.ObjectItem;

public class SecGraphImporter {
	public static Object importFromFile (Context ctx)
	{
		Workbench wb = ctx.getWorkbench ();
		FileChooserResult fr = wb.getToolkit().chooseFile (null,
			IO.getReadableFileTypes (new IOFlavor [] {IOFlavor.NODE}), Window.ADD_FILE, false, null, ctx, null); 
		if (fr == null)
		{
			return null;
		}
		MimeType mt = fr.getMimeType ();
		if (fr.files != null && fr.files.length > 1) {
			ObjectList<Object> l = new ObjectList<Object>(fr.files.length);
			for (File f : fr.files) {
				FileSource src = new FileSource(f,mt,ctx.getWorkbench().getRegistry(), new StringMap ());
				Object o = ctx.getWorkbench().readObject (src, IOFlavor.NODE);
				l.add(toGraphItem (o, ctx, src));
			}
			return l;
		}		
		FileSource src = fr.createFileSource (ctx.getWorkbench().getRegistry(), new StringMap ());
		Object o = ctx.getWorkbench().readObject (src, IOFlavor.NODE);
		if (o == null)
		{
			return null;
		} 
		return toGraphItem (o, ctx, src);
	} 
	public static Object toGraphItem (Object node, Context ctx, FileSource src) {
		if (node instanceof Node) {
			MimeType mime = GraphXMLSource.MIME_TYPE;
			Object sg = new SecGraphImpl((Node) node, Workbench.current());
			FileSystem pfs = ctx.getWorkbench().getRegistry().getFileSystem();
			String directory = "secgraphs";
			Object dir = pfs.getRoot();
			Object f = ((FileSource) src).getFile();
			String url = src.toURL().getFile();
			String fileName = url.substring(url.lastIndexOf('/') + 1);
			fileName = fileName.split("\\.")[0];
			((SecGraphImpl) sg).getRootNode();
			GraphXMLSource gxs = new GraphXMLSource(((SecGraphImpl) sg).getGraph(),
					ctx.getWorkbench().getRegistry(), null, ((SecGraphImpl) sg).getRootNode());
			StreamAdapter sad = new StreamAdapter(gxs,
					new IOFlavor(GraphXMLSource.MIME_TYPE, IOFlavor.OUTPUT_STREAM, null));
			try {
				dir = pfs.create(dir, directory, true);
			} catch (IOException e) {
				ctx.getWorkbench().logGUIInfo(IO.I18N.msg("mkdir.failed", directory), e);
				return null;
			}
			Object dest;
			try {
				if (pfs.isIn(fileName))
					dest = pfs.createIfDoesntExist(dir, fileName, false, false);
				else {
					dest = pfs.createIfDoesntExist(dir, fileName, false, false);
					pfs.addLocalFile(f, dir, pfs.getName(dest), false, false);
				}
				sad.write(pfs, dest);
			} catch (IOException e) {
				ctx.getWorkbench().logGUIInfo(IO.I18N.msg("addfile.failed", directory), e);
				return null;
			}

			ObjectItem item = new ProjectFileObjectItem(IO.toSystemId(pfs, dest), mime, sg,
					SecGraphImpl.class.getName(), SecGraphImpl.MIME_TYPE);
			return item;
		}
		return null;
	}

}
