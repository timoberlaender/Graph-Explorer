package de.grogra.graph.object.sg.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.Executor;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.sg.SecGraph;
import de.grogra.graph.object.sg.impl.exception.NoQueryGraphException;
import de.grogra.graph.object.sg.impl.exception.NoRuleGraphException;
import de.grogra.graph.object.sg.impl.io.XMLRootlessGraphReader;
import de.grogra.imp.View;
import de.grogra.imp.registry.EmbeddedFileObject;
import de.grogra.imp2d.graphs.TopologyGraph;
import de.grogra.imp2d.graphs.TopologyGraphDescriptor;
import de.grogra.imp3d.Camera;
import de.grogra.imp3d.View3D;
import de.grogra.persistence.PersistenceBindings;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.ServerConnection;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.persistence.Transaction;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.StreamAdapter;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.edit.Selectable;
import de.grogra.pf.ui.edit.Selection;
import de.grogra.pf.ui.registry.PanelFactory;
import de.grogra.pf.ui.registry.ProjectFileObjectItem;
import de.grogra.rgg.RGG;
import de.grogra.rgg.model.Compiletime;
import de.grogra.rgg.model.RGGGraph;
import de.grogra.util.Lock;
import de.grogra.util.LockProtectedRunnable;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.xl.query.EdgeDirection;
import de.grogra.xl.query.HasModel;
import de.grogra.xl.query.MatchConsumer;
import de.grogra.xl.query.Pattern;
import de.grogra.xl.query.Pattern.Matcher;
import de.grogra.xl.query.Producer;
import de.grogra.xl.query.QueryState;
import de.grogra.xl.query.RuntimeModel;
import de.grogra.xl.util.IntList;
import de.grogra.xl.util.XBitSet;
import de.grogra.graph.object.GraphContext;
import de.grogra.graph.object.impl.GraphInstance;
import de.grogra.graph.object.impl.GraphObjectDescriptor;
import de.grogra.graph.object.impl.GraphObjectSelection;
import de.grogra.graph.object.sg.*;

@HasModel(Compiletime.class)
public class SecGraphImpl extends de.grogra.imp.registry.EmbeddedSharedObject implements SecGraph, Selectable// Showable

{
	private static final long serialVersionUID = 2451430058684572298L;

	public static final MimeType MIME_TYPE = MimeType.valueOf("application/x-secgraph");

	private boolean autoRecreate = true;
	private GraphManager gm;
	private Registry registry;

	private QueryRootImpl queryRoot;
	private ProductionRootImpl productionRoot;

	private RGGGraph rggGraph;
	@Override
	public MimeType getMimeType() {
		// TODO Auto-generated method stub
		return MIME_TYPE;// MimeType.valueOf("model/secgraphXML");
	}

	@Override
	public String getResourceDirectoryName() {
		// TODO Auto-generated method stub
		return "ESOsecgraphs";
	}

	@Override
	public String getObjectType() {
		// TODO Auto-generated method stub
		return SecGraphImpl.class.getName();
	}

	SecGraphImpl() {
		super();
	}

	public SecGraphImpl(Context ctx) {
		super();
		initGraph(ctx);
		setRootNode(new Node());
	}

	public SecGraphImpl(Context ctx, Node root) {
		super();
		initGraph(ctx);
		Node r = new Node();
		setRootNode(r);
		try {
			r.addEdgeBitsTo(root.cloneGraph(EdgePatternImpl.FORWARD, true), Graph.BRANCH_EDGE, null);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public SecGraphImpl(Context ctx, GraphContext inp) {
		super();
		initGraph(ctx);
		for (String key : inp.getGraph().getRootKeys()) {
			Object r = inp.getGraph().getRoot(key);
			if (r instanceof Node) {
				Node n;
				try {
					n = ((Node) r).cloneGraph(EdgePatternImpl.FORWARD, true);
					resolve().getGraph().setRoot(key, n);
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public SecGraphImpl(Node node, Context ctx) {
		super();
		initGraph(ctx);
		setRootNode(node);
	}

	public boolean isAutoRecreate() {
		return autoRecreate;
	}

	public void view2d(Context ctx) {
		if (getGraph().getMainState().getContext() != Workbench.current().getJobManager().getThreadContext()) {
			getGraph().initMainState((Executor) Workbench.current().getJobManager());
		}

		TopologyGraphDescriptor tgd = new TopologyGraphDescriptor() {
			@Override
			public Graph getGraph(View view) {
				return new TopologyGraph(SecGraphImpl.this.getGraph(), Graph.MAIN_GRAPH);
			}
		};

		StringMap params = new StringMap();
		params.put("graph", tgd);

		View v = (View) PanelFactory.getAndShowPanel(ctx, "/ui/panels/2d/view", params);
		v.setGraph(tgd);
	}

	public void view3d(Context ctx) {
		{
			if (getGraph().getMainState().getContext() != Workbench.current().getJobManager().getThreadContext()) {
				getGraph().initMainState((Executor) Workbench.current().getJobManager());
			}
			GraphObjectDescriptor sgd = new GraphObjectDescriptor();

			View v3 = (View) PanelFactory.getAndShowPanel(ctx, "/ui/panels/3d/view", null);

			sgd.setGraphObject(this);
			v3.setGraph(sgd);
			((View3D) v3).setCamera(Camera.createPerspective());
		}

	}

	@Override
	public GraphManager getGraph() {
		return this.gm;
	}

	private void initGraph(Context ctx) {
		registry = ctx.getWorkbench().getRegistry();
		ServerConnection conn = new ServerConnection(new PersistenceBindings(registry, registry));

		gm = new GraphManager(conn, Registry.PROJECT_GRAPH, true, false);
		getGraph().initMainState((Executor) ctx.getWorkbench().getJobManager());
		rggGraph =new SecRGGGraph(de.grogra.rgg.model.Runtime.INSTANCE,getGraph());

	}

	@Override
	public void reload() {
		clear();
		ProjectFileObjectItem sop = (ProjectFileObjectItem) getProvider();
		StreamAdapter a = new StreamAdapter(sop.createFileSource(),
				new IOFlavor(GraphXMLSource.MIME_TYPE, IOFlavor.SAX, null));
		String[] keys = getGraph().getRootKeys();
		Node[] roots = new Node[keys.length];
		int i = 0;
		for (String key : keys) {
			roots[i] = (Node) getGraph().getRoot(key);
			i++;
		}
		XMLRootlessGraphReader reader = new XMLRootlessGraphReader(
				new PersistenceBindings(getRegistry(), getRegistry()), getGraph(), roots);
		try {
			a.parse(reader, null, null, null, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void clear() {
		queryRoot = null;
		productionRoot = null;

		UI.executeLockedly(getGraph(), true, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object arg, Context c) {
				Transaction xa = getGraph().getActiveTransaction();
				for (String ro : getGraph().getRootKeys()) {
					Node root = (Node) getGraph().getRoot(ro);
					for (Edge e = root.getFirstEdge(), f; e != null; e = f) {
						f = e.getNext(root);
						if ((e.testEdgeBits(Graph.BRANCH_EDGE) || (e.testEdgeBits(Graph.SUCCESSOR_EDGE)))
								&& (e.getTarget() != root)) {
							e.remove(xa);
						}
					}
				}
				xa.commitAll();
			}
		}, null, Workbench.current(), JobManager.ACTION_FLAGS);
	}

	@Override
	public void write() {
		ProjectFileObjectItem sop = (ProjectFileObjectItem) getProvider();
		Registry r = sop.getRegistry();
		GraphXMLSource gxs = new GraphXMLSource(getGraph(), r, null);
		StreamAdapter sad = new StreamAdapter(gxs,
				new IOFlavor(GraphXMLSource.MIME_TYPE, IOFlavor.OUTPUT_STREAM, null));
		Object file = r.getProjectFile(sop.getSystemId()); // memoryfilesystem$Entry
		try {
			sad.write(r.getFileSystem(), file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void apply(String cmd) {
		Item c = Item.resolveItem(Workbench.current().getRegistry(), "/workbench/rgg/methods/" + cmd);
		if (c != null && c instanceof Command) {
			de.grogra.rgg.model.Runtime.INSTANCE.setCurrentGraph(getGraph());
			UI.executeLockedly(getGraph(), true, (Command) c, null, Workbench.current(), JobManager.ACTION_FLAGS);
			de.grogra.rgg.model.Runtime.INSTANCE.currentGraph().derive();
			de.grogra.rgg.model.Runtime.INSTANCE.setCurrentGraph(Workbench.current().getRegistry().getProjectGraph());
		}
	}

	
	public RGGGraph getRGGGraph() {
		return rggGraph;
	}
	
	@Override
	public void applyXL(String qu) {
		de.grogra.rgg.model.Runtime.INSTANCE.setCurrentGraph(getGraph());

		UI.executeLockedly(getGraph(), true, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object arg, Context c) {
				XLinLineRunner ru = new XLinLineRunner();

				ru.run(qu, getRegistry());

			}
		}, null, Workbench.current(), JobManager.ACTION_FLAGS);
		de.grogra.rgg.model.Runtime.INSTANCE.setCurrentGraph(Workbench.current().getRegistry().getProjectGraph());
	}


	@Override
	public Node toInstance() {
		if (getRootNode() == null) {
			return new Node();
		}
		return new GraphInstance(this);
	}

	@Override
	public void handleSaveEvent() {
		if (getGraph().getMainState().getContext() != Workbench.current().getJobManager().getThreadContext()) {
			getGraph().initMainState((Executor) Workbench.current().getJobManager());
		}
		if (isAutoRecreate()) {
			write();
			reload();
		} else {
			write();
		}
	}

	@Override
	public void setAutoRecreate(boolean b) {
		this.autoRecreate = b;

	}

	public SecGraphInitialProducer producer$begin() {
		return (SecGraphInitialProducer) getInitalProducer();
	}

	@Override
	public Object getInitalProducer() {
		return new SecGraphInitialProducer(getRootNode());
	}

	@Override
	public void setCurrent() {
		if (getGraph().getMainState().getContext() != Workbench.current().getJobManager().getThreadContext()) {
			getGraph().initMainState((Executor) Workbench.current().getJobManager());
		}
		Lock myLock = getGraph().lock(false, true);
		myLock.retain();
		RGG.getMainRGG(Workbench.current().getRegistry()).addLock(myLock);
	}

	@Override
	public void releaseCurrent() {
		Lock myLock = RGG.getMainRGG(Workbench.current().getRegistry()).pullLock();
		try {
			getGraph().executeForcedly(new LockProtectedRunnable() {
				@Override
				public void run(boolean sync, Lock lock) {
					lock = null;
				}
			}, myLock);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block

		}
		getGraph().killLock(myLock);

	}

	public static void save(Registry r) {
		Item projectDir = Item.resolveItem(r, "/project/objects/secgraphs");
		for (Node n = projectDir.getBranch(); n != null; n = n.getSuccessor()) {
			if (n instanceof ProjectFileObjectItem) {
				Object o = ((ProjectFileObjectItem) n).getObject();
				if (o != null && o instanceof SecGraph) {
					((SecGraph) o).handleSaveEvent();
				} else {
					((SecGraph) n).handleSaveEvent();
				}
			}
			if (n instanceof SecGraph) {
				((SecGraph) n).handleSaveEvent();
			}

		}
	}

	@Override
	public Node getRootNode() {
		return getGraph().getRoot();
	}

	@Override
	public void setRootNode(Node root) {
		getGraph().setRoot(Graph.MAIN_GRAPH, root);

	}

	@Override
	public Node cloneGraph() throws CloneNotSupportedException {
		return getRootNode().cloneGraph(EdgePatternImpl.FORWARD, true);
	}

	@Override
	public Object describes() {
		StringMap nodeType = new StringMap();

		VisitorImpl v = new VisitorImpl() {
			@Override
			public Object visitEnter(Path path, boolean node) {
				if (node) {
					Node n = (Node) path.getObject(-1);
					nodeType.put(n.getNType().getName(), n.getNType());
				}
				return null;
			}

			@Override
			public Object visitInstanceEnter() {
				return STOP;
			}
		};

		v.init(getRootNode().getCurrentGraphState(), EdgePatternImpl.TREE);
		getGraph().accept(getRootNode(), v, null);

		return nodeType;
	}

	@Override
	public Node getNode(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Registry getRegistry() {
		return registry;
	}

	/*
	 * @Override public ManageableType getManageableType() { // TODO Auto-generated
	 * method stub return null; }
	 */

	@Override
	public boolean includes(SecGraph sg) throws NoQueryGraphException {
		return Library.includedIn(sg, this);

	}

	@Override
	public boolean inProjectGraph() throws NoQueryGraphException {
		return Library.inProjectGraph(this);

	}

	@Override
	public boolean includes(SecGraph sg, boolean asQuery) throws NoQueryGraphException {
		return Library.includedIn(sg, this, asQuery);
	}

	@Override
	public boolean inProjectGraph(boolean asQuery) throws NoQueryGraphException {
		return Library.inProjectGraph(this, asQuery);
	}

	@Override
	public void execute() throws NoRuleGraphException {
		Library.apply(this);
	}

	@Override
	public void execute(SecGraph sg) throws NoRuleGraphException {
		Library.apply(this, sg);
	}

	@Override
	public void apply(SecGraph rule) throws NoRuleGraphException {
		Library.apply(rule, this);
	}

	@Override
	public Node getQueryRoot(boolean create) {
		if (queryRoot != null) {
			return queryRoot;
		}
		for (Edge e = getRootNode().getFirstEdge(); e != null; e = e.getNext(getRootNode())) {
			if (e.getTarget() instanceof QueryRoot) {
				queryRoot = (QueryRootImpl) e.getTarget();
				return e.getTarget();
			}
		}
		if (create) {
			queryRoot = new QueryRootImpl();
			getRootNode().addEdgeBitsTo(queryRoot, Graph.BRANCH_EDGE, null);
			return queryRoot;
		}
		return null;
	}

	@Override
	public Node getProductionRoot(boolean create) {
		if (productionRoot != null) {
			return productionRoot;
		}
		for (Edge e = getRootNode().getFirstEdge(); e != null; e = e.getNext(getRootNode())) {
			if (e.getTarget() instanceof ProductionRoot) {
				productionRoot = (ProductionRootImpl) e.getTarget();
				return e.getTarget();
			}
		}
		if (create) {
			productionRoot = new ProductionRootImpl();
			getRootNode().addEdgeBitsTo(productionRoot, Graph.BRANCH_EDGE, null);
			return queryRoot;
		}
		return null;
	}

	@Override
	public Object query() {
		getQueryRoot(true);
		return new SecGraphInitialProducer(queryRoot);
	}

	@Override
	public Object production() {
		getProductionRoot(true);
		return new SecGraphInitialProducer(productionRoot);
	}

	@Override
	public boolean isQueryGraph() {
		return getQueryRoot(false) != null;
	}

	@Override
	public boolean isRuleGraph() {
		return isQueryGraph() && (getProductionRoot(false) != null);
	}

	@Override
	public String getName() {
		SharedObjectProvider sop = (SharedObjectProvider) getProvider();
		if (sop instanceof ProjectFileObjectItem) {
			return ((ProjectFileObjectItem) sop).getName();
		} else {
			return "unNamed";
		}
	}

	@Override
	public Selection toSelection(Context ctx) {
		return new GraphObjectSelection(this, ctx);
	}

	@Override
	public Node produceNode() {
		return toInstance();
	}

	@Override
	public SecGraph resolve() {
		// TODO Auto-generated method stub
		return this;
	}

	public Object manageableWriteReplace() {
		super.manageableWriteReplace();
		if (getProvider(false) instanceof EmbeddedFileObject) {
//			((EmbeddedFileObject)getProvider(false))
		}
		return this;
	}

	public Object clone() throws CloneNotSupportedException {
		SecGraphImpl x = new SecGraphImpl(Workbench.get(getRegistry()), this);
		return x;
	}

	public void add(GraphContext inp) {
		for (String key : inp.getGraph().getRootKeys()) {
			Object r = inp.getGraph().getRoot(key);
			if (r instanceof Node) {
				if (getGraph().getRoot(key) == null) {
					Node n;
					try {
						n = ((Node) r).cloneGraph(EdgePatternImpl.FORWARD, true);
						getGraph().setRoot(key, n);
					} catch (CloneNotSupportedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					Node realRoot = (Node) getGraph().getRoot(key);
					for (Edge e = ((Node) r).getFirstEdge(); e != null; e = e.getNext(((Node) r))) {
						if (e.getTarget() != r) {
							try {
								realRoot.addEdgeBitsTo(e.getTarget().cloneGraph(EdgePatternImpl.FORWARD, true),
										e.getEdgeBits(), null);
							} catch (CloneNotSupportedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}

					}
				}
			}
		}

	}

	@Override
	public void disposeField() {
		SharedObjectProvider s;
		if ((s = getProvider(false)) instanceof EmbeddedFileObject) {
			((EmbeddedFileObject)s).deactivate();
			((EmbeddedFileObject)s).remove();
		}
	}
	
	@Override
	public String getExtension() {
		return null;
	}

	@Override
	public boolean hasContent() {
		return true;
	}
	
	

	@Override
	public RuntimeModel getModel() {
		return rggGraph.getModel();
	}

	@Override
	public QueryState createQueryState() {
		return rggGraph.createQueryState();
	}

	@Override
	public Producer createProducer(QueryState qs) {
		return rggGraph.createProducer(qs);
	}

	@Override
	public Matcher createMatcher(Pattern pred, XBitSet providedConstants, IntList neededConstantsOut) {
		return rggGraph.createMatcher(pred, providedConstants, neededConstantsOut);
	}

	@Override
	public boolean canEnumerateNodes(de.grogra.reflect.Type type) {
		return rggGraph.canEnumerateNodes(type);
	}

	@Override
	public void enumerateNodes(de.grogra.reflect.Type type, QueryState qs, int tp, MatchConsumer consumer, int arg) {
		rggGraph.enumerateNodes(type, qs, tp, consumer, arg);
	}

	@Override
	public boolean canEnumerateEdges(EdgeDirection dir, boolean constEdge, Serializable edge) {
		return rggGraph.canEnumerateEdges(dir, constEdge, edge);
	}

	@Override
	public void enumerateEdges(Object node, EdgeDirection dir, de.grogra.reflect.Type edgeType, QueryState qs,
			int toIndex, int patternIndex, Serializable pattern, int matchIndex, MatchConsumer consumer, int arg) {
			rggGraph.enumerateEdges(node, dir, edgeType, qs, toIndex, patternIndex, pattern, matchIndex, consumer, arg);
	}

	@Override
	public Object getRoot() {
		return rggGraph.getRoot();
	}

	@Override
	public Object getTypeRoot() {
		return rggGraph.getTypeRoot();
	}

	@Override
	public void enumerateSpaces(Object node, EdgeDirection dir, de.grogra.reflect.Type edgeType, QueryState qs,
			int toIndex, int patternIndex, Serializable pattern, int matchIndex, MatchConsumer consumer, int arg) {
		// TODO Auto-generated method stub
		rggGraph.enumerateSpaces(node, dir, edgeType, qs, toIndex, patternIndex, pattern, matchIndex, consumer, arg);
	}

	@Override
	public HashMap<Object, Integer> sortedTypeGraph() {
		// TODO Auto-generated method stub
		return rggGraph.sortedTypeGraph();
	}

	public long derive () {
		return rggGraph.derive();
	}
	
	// enh:sco SCOType
	// enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (SecGraphImpl representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new SecGraphImpl ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (SecGraphImpl.class);
		$TYPE.validate ();
	}

//enh:end

}
