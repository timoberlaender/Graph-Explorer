package de.grogra.graph.object.sg.impl;

import de.grogra.graph.impl.Node;

public class QueryRootImpl extends Node implements de.grogra.graph.object.sg.QueryRoot {
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new QueryRootImpl ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new QueryRootImpl ();
	}

//enh:end
	
	@Override
	public int getSymbolColor ()
	{
		return 0x009933FF;
	}
}
