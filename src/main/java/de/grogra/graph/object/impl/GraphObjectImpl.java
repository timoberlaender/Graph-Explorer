package de.grogra.graph.object.impl;

import java.io.IOException;
import java.util.concurrent.Executor;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.GraphObject;
import de.grogra.imp.View;
import de.grogra.imp2d.graphs.TopologyGraph;
import de.grogra.imp2d.graphs.TopologyGraphDescriptor;
import de.grogra.imp3d.Camera;
import de.grogra.imp3d.View3D;
import de.grogra.persistence.ManageableType;
import de.grogra.persistence.PersistenceBindings;
import de.grogra.persistence.ServerConnection;
import de.grogra.persistence.ShareableBase;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.io.VirtualFileWriterSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.*;
import de.grogra.pf.ui.edit.Selectable;
import de.grogra.pf.ui.edit.Selection;
import de.grogra.pf.ui.registry.PanelFactory;
import de.grogra.pf.ui.registry.ProjectFileObjectItem;

public class GraphObjectImpl  extends ShareableBase implements java.io.Serializable, Selectable, GraphObject{

	public static final MimeType MIME_TYPE = MimeType.valueOf("application/x-graphobject");
	
	// enh:sco SCOType
	
	private GraphManager gm;
	protected Registry registry ;
	
	// enh:insert
	
	
	public GraphObjectImpl() {
		super();
	}
	
	public GraphObjectImpl(Node n, Context ctx) {
		super();
		initGraph(ctx);
		setRootNode(n);
		
	}
	public GraphObjectImpl(Context ctx) {
		super();
		initGraph(ctx);
	
	}	
	private void initGraph(Context ctx) {
		registry = ctx.getWorkbench().getRegistry();
		ServerConnection conn = new ServerConnection (
				new PersistenceBindings ( registry, registry));
		
		gm = new	GraphManager(conn, Registry.PROJECT_GRAPH, true, false);
		getGraph().initMainState((Executor) ctx.getWorkbench().getJobManager());
		
	}

	@Override
	public Node getRootNode() {
		return getGraph().getRoot();
	}

	@Override
	public void setRootNode(Node root) {
		getGraph().setRoot(Graph.MAIN_GRAPH, root);
	}

	@Override
	public Node cloneGraph() throws CloneNotSupportedException {
		return getRootNode().cloneGraph(EdgePatternImpl.FORWARD, true);
	}

	@Override
	public void write() {
		ProjectFileObjectItem sop = (ProjectFileObjectItem)getProvider();
		Registry r = sop.getRegistry();
		
		// Check if the file exists (do it in the export)
		Object file = r.getProjectFile(sop.getSystemId()); //memoryfilesystem$Entry
		
		FilterSource src = new ObjectSourceImpl(getRootNode(), sop.getSystemId(), IOFlavor.NODE, 
				r.getRootRegistry(), null);
		FilterSource fs = IO.createPipeline(src, new IOFlavor(sop.getFileMimeType(), IOFlavor.VFILE_WRITER, null));
		
		try {
			((VirtualFileWriterSource) fs).write(r.getFileSystem() ,src);
		} catch (IOException e) {
			Workbench.current().logGUIInfo
			(IO.I18N.msg ("openfile.failed", sop.getSystemId()));
		}
	}

	@Override
	public void reload() {
		// TODO Auto-generated method stub
		
	}


	public void view2d(Context ctx) {		
		if(getGraph().getMainState().getContext() !=Workbench.current().getJobManager().getThreadContext()) {
			getGraph().initMainState((Executor) Workbench.current().getJobManager());
		}
			TopologyGraphDescriptor tgd = new TopologyGraphDescriptor() {
				@Override
				public Graph getGraph(View view) {
					return new TopologyGraph(GraphObjectImpl.this.getGraph(), Graph.MAIN_GRAPH);
				}
			};

			StringMap params = new StringMap();
			params.put("graph", tgd);
		
			
			View v = (View) PanelFactory.getAndShowPanel(ctx, "/ui/panels/2d/view", params);
			v.setGraph(tgd);
		}
	

	public void view3d(Context ctx) {		{
		if(getGraph().getMainState().getContext() !=Workbench.current().getJobManager().getThreadContext()) {
			getGraph().initMainState((Executor) Workbench.current().getJobManager());
		}
		GraphObjectDescriptor sgd = new GraphObjectDescriptor(this);

			View v3 = (View) PanelFactory.getAndShowPanel(ctx, "/ui/panels/3d/view", null);

			v3.setGraph(sgd);
			((View3D) v3).setCamera(Camera.createPerspective());
		}

}
	
	@Override
	public String getName() {
		SharedObjectProvider sop = (SharedObjectProvider) getProvider();
		if(sop instanceof ProjectFileObjectItem) {
			return ((ProjectFileObjectItem)sop).getName();
		}else {
			return "unNamed";
		}
	}
	
	
	
	@Override
	public Object describes() {
	StringMap nodeType = new StringMap();
		
		VisitorImpl v = new VisitorImpl ()
		{
			@Override
			public Object visitEnter (Path path, boolean node)
			{
				if (node)
				{
					Node n = (Node) path.getObject (-1);
					nodeType.put(n.getNType().getName(), n.getNType());
				}
				return null;
			}

			@Override
			public Object visitInstanceEnter ()
			{
				return STOP;
			}
		};

		v.init (getRootNode().getCurrentGraphState (), EdgePatternImpl.TREE);
		getGraph ().accept (getRootNode(), v, null);
		
		return nodeType;

	}

	@Override
	public GraphManager getGraph() {
		return this.gm;
	}

	@Override
	public Node getNode(String key) {
		return getGraph().getNodeForName(getGraph().getRoot(), key);
	}

	@Override
	public ManageableType getManageableType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Registry getRegistry() {
		// TODO Auto-generated method stub
		return registry;
	}

	@Override
	public Node produceNode() {
		return new GraphInstance(this);
	}

	@Override
	public GraphObject resolve() {
		return this;
	}

	@Override
	public Selection toSelection(Context ctx) {
		return new GraphObjectSelection(this, ctx);
	}
}
