package de.grogra.graph.object.impl;


import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.GraphObject;
import de.grogra.pf.registry.ItemReference;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Context;

public class GraphObjectRef extends ItemReference<GraphObject> implements GraphObject  {

	public GraphObjectRef(String name) {
		super(name);
	}
	
	public GraphObjectRef() {
		super(null);
	}
	
	
	public synchronized GraphObject resolve() {
		return objectResolved ? object
				 : resolveObject ("/objects/graphobjects", Registry.current ());
	}

	
	//enh:sco
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends ItemReference.Type
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (GraphObjectRef representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, ItemReference.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new GraphObjectRef ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (GraphObjectRef.class);
		$TYPE.validate ();
	}

//enh:end
	
	
	

	@Override
	public Node getRootNode() {
		return resolve().getRootNode();
	}

	@Override
	public void setRootNode(Node root) {
		resolve().setRootNode(root);
		
	}

	@Override
	public Node cloneGraph() throws CloneNotSupportedException {
		return resolve().cloneGraph();
	}

	@Override
	public void write() {
		resolve ().write();
		
	}

	@Override
	public void reload() {
		resolve ().reload();
		
	}

	@Override
	public Object describes() {
		return resolve ().describes();
	}

	@Override
	public GraphManager getGraph() {
		return resolve ().getGraph();
	}

	@Override
	public Node getNode(String key) {
		return resolve ().getNode(key);
	}

	@Override
	public Registry getRegistry() {
		return resolve().getRegistry();
	}

	@Override
	public Node produceNode() {
		return resolve().produceNode();
	}

	@Override
	public void view3d(Context ctx) {
		resolve().view3d(ctx);
		
	}

	@Override
	public void view2d(Context ctx) {
		resolve().view2d(ctx);
		
	}
}
